#ifndef MYGRAPHICSITEM_H
#define MYGRAPHICSITEM_H

#include <QGraphicsPixmapItem>

class myGraphicsItem: public QGraphicsPixmapItem
{
public:
    myGraphicsItem(const QPixmap pm);

};

#endif // MYGRAPHICSITEM_H
