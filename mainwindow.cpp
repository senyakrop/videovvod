#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->srcGW->setScene(&m_srcSC);
    ui->dstGW->setScene(&m_dstSC);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_openBtn_clicked()
{
    QString fp = QFileDialog::getOpenFileName();

    m_srcmyGI = new myGraphicsItem(QPixmap(fp));
    m_dstmyGI = new myGraphicsItem(QPixmap(fp));

    m_srcSC.addItem(m_srcmyGI);
    m_dstSC.addItem(m_dstmyGI);

}


void MainWindow::on_brBtn_clicked()
{
    QImage tmp=m_srcmyGI->pixmap().toImage();

    for(int y=0; y<tmp.height(); y++)
        for(int x=0; x<tmp.width(); x++)
        {
            int r = qRed (tmp.pixel(x, y));
            int g = qGreen (tmp.pixel(x, y));
            int b = qBlue (tmp.pixel(x, y));

            int grey = (r+g+b) / 3;

            tmp.setPixel(x, y, qRgb(grey, grey, grey));
        }

    m_dstmyGI->setPixmap(QPixmap::fromImage(tmp));
}


void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    QImage tmp=m_srcmyGI->pixmap().toImage();

    for(int y=0; y<tmp.height(); y++)
        for(int x=0; x<tmp.width(); x++)
        {
            if (qRed(tmp.pixel(x, y))>value)
            {
                tmp.setPixel(x, y, qRgb(255, 255, 255));
            }
            else
            {
                tmp.setPixel(x, y, qRgb(0, 0, 0));
            }
        }
    m_dstmyGI->setPixmap(QPixmap::fromImage(tmp));
}

